﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerPro
{
    public class SpecialMovement_ChargeAttack : SpecialMovement
    {
        public float force;
        public float drag;
        public float chargeGravity = -5f;
        public float dizzyTime = 1f;

        public int actionButton = 0;
        public Transform projectilePrefab;

        protected bool isAttacking = false;
        protected bool isCharging = false;
        protected bool isDizzy = false;
        protected Vector2 pushDirection = Vector2.zero;
        protected float currentDizzyTime = 0f;

        protected ProjectileAimer projectileAimer;

        #region movement info constants and properties

        /// <summary>
        /// Human readable name.
        /// </summary>
        private const string Name = "Attack/Charge";

        /// <summary>
        /// Human readable description.
        /// </summary>
        private const string Description = "A charge attack that allows you to float in the air and select a direction.";

        /// <summary>
        /// Static movement info used by the editor.
        /// </summary>
        new public static MovementInfo Info
        {
            get
            {
                return new MovementInfo(Name, Description);
            }
        }

        #endregion

        public override Movement Init(Character character)
        {
            AssignReferences(character);
            projectileAimer = character.GetComponentInChildren < ProjectileAimer >(true);

            return this;
        }

        public override AnimationState AnimationState
        {
            get
            {
                return AnimationState.FALL;
            }
        }

        public override int AnimationPriority
        {
            get
            {
                return 1;
            }
        }

        public override bool WantsSpecialMove()
        {
            bool wantsControl = isCharging || isAttacking || isDizzy;

            if (character.Input.GetActionButtonState(actionButton) == ButtonState.DOWN && !wantsControl)
            {
                isCharging = true;
                isAttacking = false;
                isDizzy = false;
                currentDizzyTime = 0;
                wantsControl = true;
                character.SetVelocityY(0f);
                projectileAimer.gameObject.SetActive(true);
            }

            return wantsControl;
        }

        public override void DoMove()
        {
            if (character.Input.GetActionButtonState(actionButton) == ButtonState.UP && isCharging)
            {
                isCharging = false;
                isAttacking = true;
            }

            if (isCharging)
            {
                // Apply gravity if not grounded
                if (!character.Grounded)
                {
                    character.AddVelocity(0, TimeManager.FrameTime * chargeGravity, false);
                }

                // Limit to terminal
                //if (character.Velocity.y < swimTerminalVelocity) character.SetVelocityY (swimTerminalVelocity);

                // Translate
                character.Translate(0, character.Velocity.y * TimeManager.FrameTime, true);
            }

            if (isAttacking)
            {
                
                pushDirection = projectileAimer.GetAimDirection(character).normalized;

                character.SetVelocityX(-pushDirection.x * force);
                character.SetVelocityY(-pushDirection.y * force);

                isAttacking = false;
                isDizzy = true;

                Transform projectile = Instantiate<Transform>(projectilePrefab);
                projectile.position = projectileAimer.transform.GetChild(0).position;

                Projectile projectileScript = projectile.GetComponent < Projectile >();
                projectileScript.Fire(0, DamageType.NONE, pushDirection, character);

                projectileAimer.gameObject.SetActive(false);
            }

            if (isDizzy)
            {
                currentDizzyTime += TimeManager.FrameTime;

                // Apply drag (we use a friction like equation which seems to look better than a drag like one)
                if (character.Velocity.x > 0)
                {
                    character.AddVelocity(-character.Velocity.x * drag * TimeManager.FrameTime, 0, true);
                    if (character.Velocity.x < 0)
                        character.SetVelocityX(0);
                }
                else if (character.Velocity.x < 0)
                {
                    character.AddVelocity(-character.Velocity.x * drag * TimeManager.FrameTime, 0, true);
                    if (character.Velocity.x > 0)
                        character.SetVelocityX(0);
                }

                // Translate
                character.Translate(character.Velocity.x * TimeManager.FrameTime, 0, true);

                // Y Move

                // Apply gravity
                if (!character.Grounded)
                {
                    character.AddVelocity(0, TimeManager.FrameTime * character.DefaultGravity, false);
                }
                // Translate
                character.Translate(0, character.Velocity.y * TimeManager.FrameTime, true);

                if (currentDizzyTime >= dizzyTime)
                    isDizzy = false;
            }
        }

        public override bool ShouldApplyGravity
        {
            get
            {
                return false;
            }
        }
    }
}
