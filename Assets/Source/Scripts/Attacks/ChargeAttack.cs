﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerPro;

public class ChargeAttack : BasicAttacks
{
    protected float force = 10.0f;
    protected float drag = 1.0f;
    protected Vector2 pushDirection;

    protected override void StartAttack(int attackIndex)
    {
        pushDirection = projectileAimer.GetAimDirection(character).normalized;

        character.SetVelocityX(-pushDirection.x * force);
        character.SetVelocityY(-pushDirection.y * force);

        base.StartAttack(attackIndex);
    }

    public override void DoMove()
    {
        // Apply drag (we use a friction like equation which seems to look better than a drag like one)
        if (character.Velocity.x > 0)
        {
            character.AddVelocity(-character.Velocity.x * drag * TimeManager.FrameTime, 0, true);
            if (character.Velocity.x < 0)
                character.SetVelocityX(0);
        }
        else if (character.Velocity.x < 0)
        {
            character.AddVelocity(-character.Velocity.x * drag * TimeManager.FrameTime, 0, true);
            if (character.Velocity.x > 0)
                character.SetVelocityX(0);
        }

        // Translate
        character.Translate(character.Velocity.x * TimeManager.FrameTime, 0, true);

        // Y Move

        // Apply gravity
        if (!character.Grounded)
        {
            character.AddVelocity(0, TimeManager.FrameTime * character.DefaultGravity, false);
        }
        // Translate
        character.Translate(0, character.Velocity.y * TimeManager.FrameTime, true);
    }
}
