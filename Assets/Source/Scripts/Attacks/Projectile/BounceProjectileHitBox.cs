﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PlatformerPro
{
    public class BounceProjectileHitBox : ProjectileHitBox
    {
        protected int numBounces = 0;
        protected int maxBounces = 3;
        protected Transform lastHitTransform;

        protected virtual void SetBounceInfo(int maxBounces)
        {
            this.numBounces = 0;
            this.maxBounces = maxBounces;    
        }

        protected override bool DoHit(Collider2D other)
        {
            // do nothing, this hit box uses collison2d information.
            return false;
        }

        protected virtual bool DoHit(Collision2D other)
        {
            if (!hasHitCharacter && enabled)
            {
                IHurtable hurtBox = (IHurtable)other.gameObject.GetComponent(typeof(IHurtable));
                // Got a hurt box and its not ourselves
                if (hurtBox != null && !hasHitCharacter && hurtBox.Mob != character)
                {
                    if (projectile != null && destroyOnEnemyHit)
                        projectile.DestroyProjectile(true);

                    damageInfo.Direction = transform.position - other.transform.position;

                    Debug.Log("hello?");
                    hurtBox.Damage(damageInfo);
                   
                    if (!allowMultiHit)
                        hasHitCharacter = true;
                    
                    if (character is Character)
                        ((Character)character).HitEnemy(hurtBox.Mob, damageInfo);
                    
                    return true;
                }
                else
                {
                    Transform hitTransform = other.contacts[0].collider.transform;

                    if (projectile != null && hitTransform != null)
                    {
                        if (numBounces >= maxBounces)
                        {
                            projectile.DestroyProjectile(false);
                        }
                        else
                        {
                            lastHitTransform = hitTransform;
                            Vector2 reflect = Vector2.Reflect(projectile.Direction, other.contacts[0].normal).normalized;

                            projectile.UpdateMovement(reflect, projectile.GetSpeed() - 5f);
                        }
                    }
                }
            }

            return false;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            DoHit(other);
        }
    }
}
