﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Spine.Unity;

namespace PlatformerPro
{
    /// <summary>
    /// An animator that plays animations directly on a mecanim controller. Typically used for 2D sprites.
    /// </summary>
    public class SpineAnimationBridge_2D : MonoBehaviour, IAnimationBridge
    {

        #region members

        [SpineAnimation]
        public string Idle;
        [SpineAnimation]
        public string Run;
        [SpineAnimation]
        public string Walk;
        [SpineAnimation]
        public string Jump;
        [SpineAnimation]
        public string DoubleJump;
        [SpineAnimation]
        public string Airborne;
        [SpineAnimation]
        public string Fall;
        [SpineAnimation]
        public string WallSlide;
        [SpineAnimation]
        public string WallJump;
        [SpineAnimation]
        public string Slide;
        [SpineAnimation]
        public string Land;
        [SpineAnimation]
        public string Damage;

        private SkeletonAnimation skeletonAnimation;

        private Spine.AnimationState spineAnimationState;
        private Spine.Skeleton skeleton;

        /// <summary>
        /// Maps from states to animator overrides.
        /// </summary>
        public List<AnimatorControllerMapping> mappings;

        /// <summary>
        /// Lookup table of attack states to animator overrides.
        /// </summary>
        protected Dictionary<string, AnimatorOverrideController> animationStateOverrideLookup;

        protected Dictionary < AnimationState , string > spineMapping;

        /// <summary>
        /// Store default controller.
        /// </summary>
        protected RuntimeAnimatorController defaultController;

        /// <summary>
        /// Cached reference to the character.
        /// </summary>
        protected IMob myCharacter;

        /// <summary>
        /// The state currently playing.
        /// </summary>
        protected AnimationState state;

        /// <summary>
        /// The animation state that should play next.
        /// </summary>
        protected Queue<AnimationState> queuedStates;

        /// <summary>
        /// The current states priority.
        /// </summary>
        protected int priority;

        /// <summary>
        /// The queued states priority.
        /// </summary>
        protected PriorityQueue queuedPriorities;


        #if UNITY_EDITOR
        /// <summary>
        /// In the editor track state names so we can show an error message if a state is missing.
        /// </summary>
        protected List<string> editor_stateNames;
        #endif

        #endregion

        #region unity hooks

        /// <summary>
        /// Unity start hook.
        /// </summary>
        void Start()
        {
            Init();
        }

        /// <summary>
        /// Unity Update hook.
        /// </summary>
        void Update()
        {
            // If we have a new animation to play
            if (queuedStates.Count > 0)
            {
                AnimationState nextState = queuedStates.Peek();
                int nextPriority = queuedPriorities.Peek();

                if (!spineMapping.ContainsKey(nextState))
                    Debug.LogErrorFormat("{0} is not present in dictionary.", nextState);
                
                // Ensure we played the current state for at least one frame, this is to work around for Mecanim issue where calling Play isn't always playing the animation
                if (state == AnimationState.NONE || skeletonAnimation.AnimationName.Equals(spineMapping[state]) ||
                    skeletonAnimation.AnimationName.Equals(spineMapping[nextState]))
                {
                    if (skeletonAnimation.AnimationName.Equals(spineMapping[nextState]))
                    {
                        state = nextState;
                        priority = nextPriority;
                        queuedStates.Dequeue();
                        queuedPriorities.Dequeue();
                    }
                        
                    // Next animation has higher priority, play it now
                    else if (nextPriority >= priority ||
                             spineAnimationState.GetCurrent(0).IsComplete)
                    {
                        spineAnimationState.SetAnimation(0, spineMapping[nextState], true);
                        state = nextState;
                        priority = nextPriority;
                        queuedStates.Dequeue();
                        queuedPriorities.Dequeue();
                    }
                }
            }
        }

        /// <summary>
        /// Unity OnDestory hook.
        /// </summary>
        void OnDestroy()
        {
            if (myCharacter != null)
                myCharacter.ChangeAnimationState -= AnimationStateChanged;
            if (TimeManager.SafeInstance != null)
                TimeManager.SafeInstance.GamePaused -= HandleGamePaused;
            if (TimeManager.SafeInstance != null)
                TimeManager.SafeInstance.GameUnPaused -= HandleGameUnPaused;
        }

        #endregion

        #region protected methods

        /// <summary>
        /// Initialise this animation bridge.
        /// </summary>
        protected void Init()
        {
            // Get character reference
            myCharacter = (IMob)gameObject.GetComponent(typeof(IMob));
            if (myCharacter == null)
                myCharacter = (IMob)gameObject.GetComponentInParent(typeof(IMob));
            if (myCharacter == null)
                Debug.LogError("Mecanim Animation Bridge (2D) unable to find Character or Enemy reference");
            myCharacter.ChangeAnimationState += AnimationStateChanged;

            skeletonAnimation = GetComponent<SkeletonAnimation>();
            spineAnimationState = skeletonAnimation.AnimationState;
            skeleton = skeletonAnimation.Skeleton;

            spineMapping = new Dictionary<AnimationState, string>()
            {
                { AnimationState.IDLE, Idle },
                { AnimationState.WALK, Walk },
                { AnimationState.RUN, Run },
                { AnimationState.SLIDE, Slide },
                { AnimationState.JUMP, Jump },
                { AnimationState.DOUBLE_JUMP, DoubleJump },
                { AnimationState.AIRBORNE, Airborne },
                { AnimationState.FALL, Fall },
                { AnimationState.WALL_SLIDE, WallSlide },
                { AnimationState.WALL_JUMP, WallJump },
                { AnimationState.LAND, Land },
                { AnimationState.HURT_NORMAL, Damage }
            };

            skeletonAnimation.AnimationName = spineMapping[AnimationState.IDLE];
            animationStateOverrideLookup = new Dictionary<string, AnimatorOverrideController>();
            foreach (AnimatorControllerMapping mapping in mappings)
            {
                animationStateOverrideLookup.Add(mapping.overrrideState, mapping.controller);
            }

            queuedStates = new Queue<AnimationState>();
            queuedPriorities = new PriorityQueue();
            state = AnimationState.NONE;
            priority = -1;

            TimeManager.Instance.GamePaused += HandleGamePaused;
            TimeManager.Instance.GameUnPaused += HandleGameUnPaused;


        }

        /// <summary>
        /// Handles the game being unpaused.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        virtual protected void HandleGameUnPaused(object sender, System.EventArgs e)
        {
            skeletonAnimation.timeScale = 1f;
        }


        /// <summary>
        /// Handles the game being paused.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">E.</param>
        virtual protected void HandleGamePaused(object sender, System.EventArgs e)
        {
            skeletonAnimation.timeScale = 0f;
        }

        /// <summary>
        /// Handles animation state changed.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="args">Arguments.</param>
        protected void AnimationStateChanged(object sender, AnimationEventArgs args)
        {
//            Debug.LogFormat("Trying to queue: {0} Queued states: {1}", args.State, queuedStates.Count < 1);
            // Don't queue states that are already queued
            if (queuedStates.Count < 1 || queuedStates.Peek() != args.State)
            {
                queuedStates.Enqueue(args.State);
                queuedPriorities.EnqueueAndPromote(args.Priority);
            }
        }

        #endregion

        #region public methods and properties

        virtual public Animator Animator
        {
            get { return null; }
        }

        /// <summary>
        /// Reset the animation state.
        /// </summary>
        virtual public void Reset()
        {
            // nothing
        }

        #endregion
    }
}