﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerPro;

public class MouseDirectionFacer : MonoBehaviour
{

    /// <summary>
    /// Should left = -1 or left = 1?
    /// </summary>
    [Tooltip("Should left = -1 (false) or left = 1 (true)?")]
    public bool flipLeftAndRight;

    /// <summary>
    /// If the sprite x position is non zero should we inverse it on switch?
    /// </summary>
    [Tooltip("If the sprite x position is non zero should we inverse it on switch?")]
    public bool flipSpriteOffset;

    /// <summary>
    /// The character reference.
    /// </summary>
    protected IMob animatable;

    /// <summary>
    /// The cached scale.
    /// </summary>
    protected Vector3 cachedScale;

    /// <summary>
    /// The cached offset.
    /// </summary>
    protected Vector3 cachedOffset;

    protected Vector2 mousePosition
    {
        get { return Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition); }
    }

    void Start()
    {
        // This is not elegant but its a simple and effective way to handle interfaces in Unity
        animatable = (IMob)gameObject.GetComponentInParent(typeof(IMob));

        if (animatable == null)
            Debug.LogError("UnitySpriteDirectionFacer can't find the animatable reference");
        
        cachedScale = transform.localScale;
        cachedOffset = transform.localPosition;
    }

    void Update()
    {
        if (enabled && !TimeManager.Instance.Paused)
        {
            float xDir = (transform.position.x > mousePosition.x) ? -1 : 1;
            transform.localScale = new Vector3((flipLeftAndRight ? -xDir : xDir) * cachedScale.x, cachedScale.y, cachedScale.z);
        }
    }
}
