﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlatformerPro;

public class Aim : MonoBehaviour
{
    public int aimDirections = 8;
    public float threshold = 0.5f;

    protected Character character;
    protected CharacterConfiguration configuration;

    protected ProjectileAimer aimer;

    private void Start()
    {
        character = GetComponentInParent < Character >();
        configuration = GetComponentInParent < CharacterConfiguration >();
        aimer = GetComponent < ProjectileAimer >();
    }

    private void Update()
    {
        if (enabled && !TimeManager.Instance.Paused)
        {
           
            // float verticalAxis = character.Input.VerticalAxis;
            // float horizontalAxis = character.Input.HorizontalAxis;

            //  if (Mathf.Abs(verticalAxis) > threshold || Mathf.Abs(horizontalAxis) > threshold)
            {
                Vector2 aimDirection = aimer.GetAimDirection(character);
                float angle = Mathf.Atan2(aimDirection.y, aimDirection.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }

        }
    }

}
