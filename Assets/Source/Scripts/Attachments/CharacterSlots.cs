﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;
using Spine.Unity.Modules.AttachmentTools;

public class CharacterSlots : MonoBehaviour
{
    [System.Serializable]
    public class SlotRegionPair
    {
        [SpineSlot]
        public string slot;

        [SpineAtlasRegion]
        public string region;
    }

    public AtlasAsset atlasAsset;
    public SlotRegionPair[] attachments;

    Atlas atlas;

    void Awake()
    {
        GetComponent<SkeletonRenderer>().OnRebuild += Apply;


    }

    void Apply(SkeletonRenderer skeletonRenderer)
    {
        atlas = atlasAsset.GetAtlas();
        float scale = skeletonRenderer.skeletonDataAsset.scale;

        var enumerator = attachments.GetEnumerator();
        while (enumerator.MoveNext())
        {
            var entry = (SlotRegionPair)enumerator.Current;

            var slot = skeletonRenderer.skeleton.FindSlot(entry.slot);
            var region = atlas.FindRegion(entry.region);
            slot.Attachment = region.ToRegionAttachment(entry.region, scale);
        }
    }
}
