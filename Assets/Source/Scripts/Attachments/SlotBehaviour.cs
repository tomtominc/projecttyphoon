﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using Spine;

public class SlotBehaviour : MonoBehaviour
{
    [SpineSlot]
    public string airBallSlot;
    private SkeletonRenderer skeletonRenderer;

    public void Start()
    {
        skeletonRenderer = GetComponent < SkeletonRenderer >();
        Slot slot = skeletonRenderer.skeleton.FindSlot(airBallSlot);
    }
}
