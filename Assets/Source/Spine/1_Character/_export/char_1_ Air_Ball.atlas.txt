
char_1_ Air_Ball.png
size: 128,128
format: RGBA8888
filter: Linear,Linear
repeat: none
airball
  rotate: false
  xy: 47, 70
  size: 22, 22
  orig: 22, 22
  offset: 0, 0
  index: -1
char1_body
  rotate: false
  xy: 2, 27
  size: 27, 20
  orig: 27, 20
  offset: 0, 0
  index: -1
char1_foot
  rotate: false
  xy: 2, 2
  size: 11, 4
  orig: 11, 4
  offset: 0, 0
  index: -1
char1_foot2
  rotate: false
  xy: 2, 2
  size: 11, 4
  orig: 11, 4
  offset: 0, 0
  index: -1
char1_hand1
  rotate: false
  xy: 47, 51
  size: 16, 17
  orig: 16, 17
  offset: 0, 0
  index: -1
char1_head
  rotate: false
  xy: 2, 49
  size: 43, 43
  orig: 43, 43
  offset: 0, 0
  index: -1
char1_land2
  rotate: false
  xy: 71, 75
  size: 11, 17
  orig: 11, 17
  offset: 0, 0
  index: -1
char1_leg1
  rotate: false
  xy: 2, 8
  size: 20, 17
  orig: 20, 17
  offset: 0, 0
  index: -1
char1_leg2
  rotate: false
  xy: 31, 30
  size: 12, 17
  orig: 12, 17
  offset: 0, 0
  index: -1
